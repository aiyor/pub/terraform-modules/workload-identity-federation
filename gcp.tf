# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

resource "google_iam_workload_identity_pool" "this" {
  count                     = local.create_gcp_wif ? 1 : 0
  workload_identity_pool_id = var.gcp_wif.wif_pool.id
  display_name              = try(var.gcp_wif.wif_pool.display_name, null)
  description               = try(var.gcp_wif.wif_pool.description, null)
  disabled                  = try(var.gcp_wif.wif_pool.disabled, null)
  project                   = var.gcp_project
}

data "google_iam_workload_identity_pool" "this" {
  count                     = local.enable_gcp_wif && !local.create_gcp_wif ? 1 : 0
  workload_identity_pool_id = var.gcp_wif.wif_pool.id
  project                   = var.gcp_project
  provider                  = google-beta
}

resource "google_service_account" "this" {
  count        = local.create_gcp_service_account ? 1 : 0
  account_id   = var.gcp_wif.service_account.id
  display_name = try(var.gcp_wif.service_account.display_name, null)
  disabled     = var.gcp_wif.service_account.disabled
  project      = var.gcp_wif.service_account.project
}

data "google_service_account" "this" {
  count      = local.enable_gcp_service_account && !local.create_gcp_service_account ? 1 : 0
  account_id = var.gcp_wif.service_account.id
  project    = var.gcp_wif.service_account.project
}

resource "google_service_account_iam_binding" "this" {
  count              = local.enable_gcp_service_account ? 1 : 0
  service_account_id = local.gcp_service_account.id
  role               = "roles/iam.workloadIdentityUser"

  members = [
    "principalSet://iam.googleapis.com/${local.gcp_wif_pool.name}/*"
  ]
}
