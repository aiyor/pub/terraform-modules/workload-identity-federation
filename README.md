# Terraform Module - `workload-identity-federation`

This Terraform module can be used to deploy an opinionated Workload Identity Federation configuration to enable pipelines (Gitlab CI and/or Github ACtions) to have keyless access across multiple clouds (AWS, Azure, GCP).

## AWS WIF Notes

This module will always create an AWS role to trust the associated OIDC provider(s) (in this case, either Gitlab or Github or both).  This is because a role's trust policy is created at the same time as the role - i.e., a trust policy cannot be attached to a role like a managed policy does. See https://docs.aws.amazon.com/cli/latest/reference/iam/attach-role-policy.html#description, and https://docs.aws.amazon.com/IAM/latest/APIReference/API_CreateRole

This module provides a default IAM policy for the role(s) created, which by default allows the role to have `sts:AssumeRole` to `all` resources.  This can be overridden by defining a customised IAM policy using the input `iam_policy`. Alternatively, if more granular customisation is needed, create a role from the caller module using the OIDC provider ARN output of this module as the trusted principal.

## Azure WIF Notes

In Azure, OpenID federated trust is configured within a service principal construct.  Azure **does not** support filter/matching rules on JWT claims.  This means each JWT subject requires a dedicated federated trust configuration. Keep this in mind when designing for Azure, as this is different to how GCP and AWS work.

## GCP WIF Notes

GCP WIF configuration is very flexible and powerful, as it allows two separate layers of controls and also support very flexible JWT claim matching rules.

However, in order to keep it uniform across Azure, AWS and GCP, this module intentionally not expose GCP's full features in WIF.  It can be considered to enable it future version if it deemed necessary.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 3.45.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.64.0 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | >= 4.64.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.0 |
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 3.45.0 |
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.64.0 |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | >= 4.64.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.github](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_openid_connect_provider.gitlab](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_policy.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.github](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.gitlab](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.github](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.gitlab](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [azurerm_federated_identity_credential.github](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/federated_identity_credential) | resource |
| [azurerm_federated_identity_credential.gitlab](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/federated_identity_credential) | resource |
| [azurerm_user_assigned_identity.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/user_assigned_identity) | resource |
| [google-beta_google_iam_workload_identity_pool_provider.github](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_iam_workload_identity_pool_provider) | resource |
| [google-beta_google_iam_workload_identity_pool_provider.gitlab](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_iam_workload_identity_pool_provider) | resource |
| [google_iam_workload_identity_pool.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool) | resource |
| [google_service_account.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_iam_binding.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_binding) | resource |
| [aws_iam_openid_connect_provider.github](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_openid_connect_provider) | data source |
| [aws_iam_openid_connect_provider.gitlab](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_openid_connect_provider) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.github_assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.gitlab_assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [azurerm_user_assigned_identity.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/user_assigned_identity) | data source |
| [google-beta_google_iam_workload_identity_pool.this](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/data-sources/google_iam_workload_identity_pool) | data source |
| [google_service_account.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/service_account) | data source |
| [tls_certificate.github](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate) | data source |
| [tls_certificate.gitlab](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_wif"></a> [aws\_wif](#input\_aws\_wif) | Configuration block to configure AWS Workload Identity Federation. | <pre>object({<br>    gitlab_role_name = optional(string, "gitlab-wif-role")<br>    github_role_name = optional(string, "github-wif-role")<br>    policy_name      = optional(string, "wif-assume-role-policy")<br>    create_new       = optional(bool, true)<br>  })</pre> | `null` | no |
| <a name="input_azure_wif"></a> [azure\_wif](#input\_azure\_wif) | Configuration block to configure Azure Workload Identity Federation. | <pre>object({<br>    service_account_type = optional(string, "managed_identity")<br>    service_account = object({<br>      name       = string<br>      create_new = optional(bool, true)<br>      location   = optional(string, null)<br>    })<br>    resource_group_name = string<br>  })</pre> | `null` | no |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The GCP Project ID. Default to null - use provider project. | `string` | `null` | no |
| <a name="input_gcp_wif"></a> [gcp\_wif](#input\_gcp\_wif) | Configuration block to configure GCP Workload Identity Federation. | <pre>object({<br>    wif_pool = optional(object({<br>      id           = string<br>      create_new   = optional(bool, true)<br>      display_name = optional(string)<br>      description  = optional(string)<br>      disabled     = optional(bool, false)<br>    }), null)<br>    service_account = optional(object({<br>      id           = string<br>      create_new   = optional(bool, true)<br>      display_name = optional(string)<br>      description  = optional(string)<br>      disabled     = optional(bool, false)<br>      project      = optional(string, null)<br>    }), null)<br>  })</pre> | `null` | no |
| <a name="input_github_oidc_defaults"></a> [github\_oidc\_defaults](#input\_github\_oidc\_defaults) | A map of default Github OIDC configuration. | <pre>object({<br>    oidc_name       = optional(string, "github")<br>    oidc_issuer     = optional(string, "https://token.actions.githubusercontent.com")<br>    oidc_audience   = optional(string, "https://github.com")<br>    organization    = optional(string, "")<br>    repository_path = string<br>    repository_id   = optional(string)<br>    ref_type        = optional(string, "branch")<br>    ref_name        = optional(string, "main")<br>  })</pre> | `null` | no |
| <a name="input_gitlab_oidc_defaults"></a> [gitlab\_oidc\_defaults](#input\_gitlab\_oidc\_defaults) | A map of default Gitlab OIDC configuration. | <pre>object({<br>    oidc_name      = optional(string, "gitlab")<br>    oidc_issuer    = optional(string, "https://gitlab.com")<br>    oidc_audience  = optional(string, "https://gitlab.com")<br>    namespace_path = optional(string)<br>    namespace_id   = optional(string)<br>    project_id     = optional(string)<br>    project_path   = string<br>    ref_type       = optional(string, "branch")<br>    ref_name       = optional(string, "main")<br>  })</pre> | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A mapping of tags/labels which should be assigned to resources. | `map(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_github_role_attributes"></a> [aws\_github\_role\_attributes](#output\_aws\_github\_role\_attributes) | Attributes for AWS Role associated with Github WIF turst. |
| <a name="output_aws_gitlab_role_attributes"></a> [aws\_gitlab\_role\_attributes](#output\_aws\_gitlab\_role\_attributes) | Attributes for AWS Role associated with Gitlab WIF turst. |
| <a name="output_aws_wif_github_provider"></a> [aws\_wif\_github\_provider](#output\_aws\_wif\_github\_provider) | Attributes for AWS Workload Identity Federation provider (OpenID Provider) for Github |
| <a name="output_aws_wif_gitlab_provider"></a> [aws\_wif\_gitlab\_provider](#output\_aws\_wif\_gitlab\_provider) | Attributes for AWS Workload Identity Federation provider (OpenID Provider) for Gitlab |
| <a name="output_azure_service_account_attributes"></a> [azure\_service\_account\_attributes](#output\_azure\_service\_account\_attributes) | Attributes for Azure Service Principal used for Workload Identity Federation. Refer to https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/user_assigned_identity#attributes-reference |
| <a name="output_gcp_service_account_attributes"></a> [gcp\_service\_account\_attributes](#output\_gcp\_service\_account\_attributes) | Attributes for GCP Service Account used for Workload Identity Federation impersonation.  Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference |
| <a name="output_gcp_wif_pool_attributes"></a> [gcp\_wif\_pool\_attributes](#output\_gcp\_wif\_pool\_attributes) | Attributes for GCP Workload Identity Federation Pool. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference |
| <a name="output_gcp_wif_provider_github"></a> [gcp\_wif\_provider\_github](#output\_gcp\_wif\_provider\_github) | Attributes for GCP Workload Identity Provider configured for Github CI. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference |
| <a name="output_gcp_wif_provider_gitlab"></a> [gcp\_wif\_provider\_gitlab](#output\_gcp\_wif\_provider\_gitlab) | Attributes for GCP Workload Identity Provider configured for Gitlab CI. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
