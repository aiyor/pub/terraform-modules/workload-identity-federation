# User TFVAR file to manage variable values when testing.
# TFVAR file(s) used in test should not be checked into git.

variable "azure_rg" {
  default = null
}

variable "azure_location" {
  default = null
}
