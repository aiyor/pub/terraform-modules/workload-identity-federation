provider "azurerm" {
  features {}
}

module "az_01" {
  source = "../../"

  azure_wif = {
    service_account_type = "managed_identity"
    service_account = {
      name       = "somethingnew"
      create_new = true
      location   = var.azure_location
    }
    resource_group_name = var.azure_rg
  }

  github_oidc_defaults = {
    oidc_name       = "az-github-01"
    repository_path = "my-github-org/my-repo"
    ref_type        = "tag"
    ref_name        = "v1.0.0"
  }

  gitlab_oidc_defaults = {
    oidc_name    = "az-gitlab-01"
    project_path = "gitlab-project-path/public"
  }

  tags = {
    category = "testing"
  }
}

module "az_02" {
  source = "../../"

  azure_wif = {
    service_account_type = "managed_identity"
    service_account = {
      name       = "somethingnew"
      location   = var.azure_location
      create_new = false
    }
    resource_group_name = var.azure_rg
  }

  github_oidc_defaults = {
    oidc_name       = "az-github-02"
    repository_path = "my-account/my-repo"
    ref_type        = "tag"
    ref_name        = "v1.0.0"
  }

  gitlab_oidc_defaults = {
    oidc_name    = "az-gitlab-02"
    project_path = "gitlab-project-path/public-02"
  }

  depends_on = [module.az_01] # any changes in module.az_01 will result in module.az_02 redeploy
}
