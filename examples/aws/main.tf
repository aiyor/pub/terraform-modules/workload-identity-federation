provider "aws" {}
provider "azurerm" {
  features {}
  skip_provider_registration = true
}

module "aws_01" {
  source = "../../"

  aws_wif = {
    create_new = true
  }

  gitlab_oidc_defaults = {
    project_path = "gitlab-project-path/private"
  }

  github_oidc_defaults = {
    repository_path = "my-github-org/my-repo"
  }
}

module "aws_02" {
  source = "../../"

  aws_wif = {
    create_new       = false
    github_role_name = "test-wif-github"
    gitlab_role_name = "test-wif-gitlab"
    policy_name      = "test-wif-policy"
  }

  gitlab_oidc_defaults = {
    project_path = "gitlab-project-path/private"
  }

  github_oidc_defaults = {
    repository_path = "my-github-org/my-repo"
  }

  depends_on = [module.aws_01] # any changes in module.aws_01 will result in module.aws_02 redeploy
}
