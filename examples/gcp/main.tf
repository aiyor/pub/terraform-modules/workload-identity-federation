module "test_01" {
  source = "../../"

  gcp_wif = {
    wif_pool = {
      id           = "test-wif-pool-01"
      create_new   = true
      display_name = "This is just a test"
      description  = "Some description"
    }
    service_account = {
      id         = "test-wif-code"
      create_new = true
    }
  }

  gitlab_oidc_defaults = {
    project_path = "gitlab-project-path/private"
  }

  github_oidc_defaults = {
    repository_path = "my-github-org/my-repo"
  }
}

module "test_02" {
  source = "../../"

  gcp_wif = {
    wif_pool = {
      id         = var.existing_gcp_wif_pool
      create_new = false
    }
  }

  gitlab_oidc_defaults = {
    oidc_name    = "gitlab-2"
    project_path = "gitlab-project-path/public"
  }
}

# module "test_03" {
#   source = "../../"

#   gcp_wif = {
#     wif_pool = {
#       id         = var.existing_gcp_wif_pool
#       create_new = false
#     }
#     service_account = {
#       id         = var.existing_gcp_sa
#       create_new = false
#     }
#   }

#   github_oidc_defaults = {
#     repository_path = "my-github-org/my-repo"
#     ref_type        = "tag"
#     ref_name        = "v1.0.0"
#   }
# }


# module "test_04" {
#   source = "../../"

#   gcp_wif = {
#     wif_pool = {
#       id         = var.existing_gcp_wif_pool
#       create_new = false
#     }
#     service_account = {
#       id         = "test-123"
#       create_new = true
#       project    = var.external_gcp_project
#     }
#   }
# }

# module "test_05" {
#   source = "../../"

#   gcp_wif = {
#     wif_pool = {
#       id         = var.existing_gcp_wif_pool
#       create_new = false
#     }
#     service_account = {
#       id         = var.external_existing_sa
#       create_new = false
#       project    = var.external_gcp_project
#     }
#   }
# }
