# User TFVAR file to manage variable values when testing.
# TFVAR file(s) used in test should not be checked into git.

variable "external_gcp_project" {
  default = null
}

variable "external_existing_sa" {
  default = null
}

variable "existing_gcp_sa" {
  default = null
}

variable "existing_gcp_wif_pool" {
  default = null
}

variable "azure_rg" {
  default = null
}

variable "azure_location" {
  default = null
}
