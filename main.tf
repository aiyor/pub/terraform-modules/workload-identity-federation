# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

locals {
  # GCP WIF
  enable_gcp_wif             = var.gcp_wif != null ? true : false
  create_gcp_wif             = try(var.gcp_wif.wif_pool.create_new, false)
  enable_gcp_service_account = try(var.gcp_wif.service_account, null) != null ? true : false
  create_gcp_service_account = try(var.gcp_wif.service_account.create_new, false)
  gcp_wif_pool               = local.create_gcp_wif ? try(google_iam_workload_identity_pool.this[0], null) : try(data.google_iam_workload_identity_pool.this[0], null)
  gcp_service_account        = local.create_gcp_service_account ? try(google_service_account.this[0], null) : try(data.google_service_account.this[0], null)

  # Azure WIF
  is_azure_mi      = try(var.azure_wif.service_account_type == "managed_identity", false)
  is_azure_app     = try(var.azure_wif.service_account_type == "application", false)
  create_azure_mi  = local.is_azure_mi ? try(var.azure_wif.service_account.create_new, false) : false
  create_azure_app = local.is_azure_app ? try(var.azure_wif.service_account.create_new, false) : false

  azure_mi = (
    local.create_azure_mi ?
    try(merge(azurerm_user_assigned_identity.this[0], { service_account_type = "managed_identity" }), null) :
    try(merge(data.azurerm_user_assigned_identity.this[0], { service_account_type = "managed_identity" }), null)
  )
  azure_app = {} # place holder for Azure application type
  azure_service_account = (
    local.is_azure_mi ? local.azure_mi :
    local.is_azure_app ? null : # to add later
    null
  )

  # AWS WIF
  enable_aws_wif      = var.aws_wif != null ? true : false
  create_aws_wif      = try(var.aws_wif.create_new, false)
  aws_gitlab_provider = local.enable_aws_wif && local.create_aws_wif ? try(aws_iam_openid_connect_provider.gitlab[0], null) : try(data.aws_iam_openid_connect_provider.gitlab[0], null)
  aws_github_provider = local.enable_aws_wif && local.create_aws_wif ? try(aws_iam_openid_connect_provider.github[0], null) : try(data.aws_iam_openid_connect_provider.github[0], null)

}
