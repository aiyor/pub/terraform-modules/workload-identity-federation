# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

locals {
  gitlab_oidc_subject = var.gitlab_oidc_defaults != null ? "project_path:${try(var.gitlab_oidc_defaults.project_path, {})}:ref_type:${try(var.gitlab_oidc_defaults.ref_type, "")}:ref:${try(var.gitlab_oidc_defaults.ref_name, "")}" : ""
}

# Get TLS certificate thumbprints
# This is only really required for AWS OpenID provider creation, however, this module
# will get the thumbprints, as long as both Gitlab and Github defaults are defined,
# regardless of whether an AWS OpenID is required or not.
data "tls_certificate" "gitlab" {
  count = var.gitlab_oidc_defaults != null ? 1 : 0
  url   = var.gitlab_oidc_defaults.oidc_issuer
}

# GCP WIF
resource "google_iam_workload_identity_pool_provider" "gitlab" {
  count                              = local.enable_gcp_wif && var.gitlab_oidc_defaults != null ? 1 : 0
  workload_identity_pool_id          = local.gcp_wif_pool.workload_identity_pool_id
  workload_identity_pool_provider_id = var.gitlab_oidc_defaults.oidc_name
  attribute_mapping = {
    "google.subject"           = "assertion.sub"
    "attribute.namespace_id"   = "assertion.namespace_id"
    "attribute.namespace_path" = "assertion.namespace_path"
    "attribute.project_id"     = "assertion.project_id"
    "attribute.project_path"   = "assertion.project_path"
    "attribute.ref_type"       = "assertion.ref_type"
    "attribute.ref"            = "assertion.ref"
    "attribute.environment"    = "assertion.environment"
  }
  oidc {
    issuer_uri        = var.gitlab_oidc_defaults.oidc_issuer
    allowed_audiences = [var.gitlab_oidc_defaults.oidc_audience]
  }
  attribute_condition = "attribute.subject==\"${local.gitlab_oidc_subject}\""

  provider = google-beta
}

# Azure WIF
resource "azurerm_federated_identity_credential" "gitlab" {
  count               = local.is_azure_mi && var.gitlab_oidc_defaults != null ? 1 : 0
  name                = var.gitlab_oidc_defaults.oidc_name
  resource_group_name = var.azure_wif.resource_group_name
  audience            = [var.gitlab_oidc_defaults.oidc_audience]
  issuer              = var.gitlab_oidc_defaults.oidc_issuer
  parent_id           = local.azure_service_account.id
  subject             = local.gitlab_oidc_subject
}


# AWS WIF
resource "aws_iam_openid_connect_provider" "gitlab" {
  count           = local.create_aws_wif && var.gitlab_oidc_defaults != null ? 1 : 0
  url             = var.gitlab_oidc_defaults.oidc_issuer
  client_id_list  = [var.gitlab_oidc_defaults.oidc_audience]
  thumbprint_list = [data.tls_certificate.gitlab[0].certificates[0].sha1_fingerprint]
}

data "aws_iam_openid_connect_provider" "gitlab" {
  count = (local.enable_aws_wif && !local.create_aws_wif && var.gitlab_oidc_defaults != null) ? 1 : 0
  url   = var.gitlab_oidc_defaults.oidc_issuer
}
