# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

resource "azurerm_user_assigned_identity" "this" {
  count               = local.create_azure_mi ? 1 : 0
  name                = var.azure_wif.service_account.name
  resource_group_name = var.azure_wif.resource_group_name
  location            = var.azure_wif.service_account.location
  tags                = var.tags
}

data "azurerm_user_assigned_identity" "this" {
  count               = local.is_azure_mi && !local.create_azure_mi ? 1 : 0
  name                = var.azure_wif.service_account.name
  resource_group_name = var.azure_wif.resource_group_name
}
