# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

terraform {
  required_version = ">= 1.4"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.45.0"
    }

    google = {
      source  = "hashicorp/google"
      version = ">= 4.64.0"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.64.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0"
    }
  }
}
