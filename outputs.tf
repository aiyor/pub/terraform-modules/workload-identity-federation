# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

output "gcp_wif_pool_attributes" {
  description = "Attributes for GCP Workload Identity Federation Pool. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference"
  value       = local.gcp_wif_pool
}

output "gcp_service_account_attributes" {
  description = "Attributes for GCP Service Account used for Workload Identity Federation impersonation.  Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference"
  value       = local.gcp_service_account
}

output "gcp_wif_provider_gitlab" {
  description = "Attributes for GCP Workload Identity Provider configured for Gitlab CI. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference"
  value       = try(google_iam_workload_identity_pool_provider.gitlab, null)
}

output "gcp_wif_provider_github" {
  description = "Attributes for GCP Workload Identity Provider configured for Github CI. Refer to https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool#attributes-reference"
  value       = try(google_iam_workload_identity_pool_provider.github, null)
}

# Azure service account attribute output contains additional custom
# attribute 'type' to indicate if the service account is Managed Identity
# or Application type
output "azure_service_account_attributes" {
  description = "Attributes for Azure Service Principal used for Workload Identity Federation. Refer to https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/user_assigned_identity#attributes-reference"
  value       = local.azure_service_account
}

output "aws_wif_github_provider" {
  description = "Attributes for AWS Workload Identity Federation provider (OpenID Provider) for Github"
  value       = local.aws_github_provider
}

output "aws_wif_gitlab_provider" {
  description = "Attributes for AWS Workload Identity Federation provider (OpenID Provider) for Gitlab"
  value       = local.aws_gitlab_provider
}

output "aws_gitlab_role_attributes" {
  description = "Attributes for AWS Role associated with Gitlab WIF turst."
  value       = try(aws_iam_role.gitlab[0], null)
}

output "aws_github_role_attributes" {
  description = "Attributes for AWS Role associated with Github WIF turst."
  value       = try(aws_iam_role.github[0], null)
}
