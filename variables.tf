# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

variable "tags" {
  description = "A mapping of tags/labels which should be assigned to resources."
  type        = map(string)
  default     = null
}

variable "gcp_wif" {
  description = "Configuration block to configure GCP Workload Identity Federation."
  type = object({
    wif_pool = optional(object({
      id           = string
      create_new   = optional(bool, true)
      display_name = optional(string)
      description  = optional(string)
      disabled     = optional(bool, false)
    }), null)
    service_account = optional(object({
      id           = string
      create_new   = optional(bool, true)
      display_name = optional(string)
      description  = optional(string)
      disabled     = optional(bool, false)
      project      = optional(string, null)
    }), null)
  })
  default = null
}

variable "gcp_project" {
  description = "The GCP Project ID. Default to null - use provider project."
  type        = string
  default     = null
}

variable "azure_wif" {
  description = "Configuration block to configure Azure Workload Identity Federation."
  type = object({
    service_account_type = optional(string, "managed_identity")
    service_account = object({
      name       = string
      create_new = optional(bool, true)
      location   = optional(string, null)
    })
    resource_group_name = string
  })
  default = null

  validation {
    condition     = var.azure_wif == null || can(contains(["managed_identity", "application"], var.azure_wif.service_account_type))
    error_message = "'azure_wif.type' must be either 'managed_identity' or 'application'"
  }
}

# Object Attributes:
#  * `gitlab_role_name`: (Optional) The name of the AWS assume role for Gitlab WIF
#  * `github_role_name`: (Optional) The name of the AWS assume role for Github WIF
#  * `custom_policy`: (Optional) Customised IAM policy for the WIF assume role(s)
#  * `create_new`: (Optional) `true` (default) to create new OIDC provider configuration or `false` to use existing
variable "aws_wif" {
  description = "Configuration block to configure AWS Workload Identity Federation."
  type = object({
    gitlab_role_name = optional(string, "gitlab-wif-role")
    github_role_name = optional(string, "github-wif-role")
    policy_name      = optional(string, "wif-assume-role-policy")
    create_new       = optional(bool, true)
  })
  default = null
}

variable "gitlab_oidc_defaults" {
  description = "A map of default Gitlab OIDC configuration."
  type = object({
    oidc_name      = optional(string, "gitlab")
    oidc_issuer    = optional(string, "https://gitlab.com")
    oidc_audience  = optional(string, "https://gitlab.com")
    namespace_path = optional(string)
    namespace_id   = optional(string)
    project_id     = optional(string)
    project_path   = string
    ref_type       = optional(string, "branch")
    ref_name       = optional(string, "main")
  })
  default = null
}

variable "github_oidc_defaults" {
  description = "A map of default Github OIDC configuration."
  type = object({
    oidc_name       = optional(string, "github")
    oidc_issuer     = optional(string, "https://token.actions.githubusercontent.com")
    oidc_audience   = optional(string, "https://github.com")
    organization    = optional(string, "")
    repository_path = string
    repository_id   = optional(string)
    ref_type        = optional(string, "branch")
    ref_name        = optional(string, "main")
  })
  default = null
}
