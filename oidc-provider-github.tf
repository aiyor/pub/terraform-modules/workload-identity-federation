# Copyright (c) Tze Liang (https://gitlab.com/tze)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

locals {
  github_oidc_ref_type = (
    try(var.github_oidc_defaults.ref_type, "") == "branch" ? "branch" :
    try(var.github_oidc_defaults.ref_type, "") == "tag" ? "tag" : ""
  )
  github_oidc_ref_name = var.github_oidc_defaults != null ? try(var.github_oidc_defaults.ref_name, "") : ""
  github_oidc_ref_string = (
    local.github_oidc_ref_type == "branch" ? "ref:refs/heads/${local.github_oidc_ref_name}" :
    local.github_oidc_ref_type == "tag" ? "ref:refs/tags/${local.github_oidc_ref_name}" : ""
  )
  github_oidc_subject = var.github_oidc_defaults != null ? "repo:${try(var.github_oidc_defaults.repository_path, {})}:${local.github_oidc_ref_string}" : ""
}

# Get TLS certificate thumbprints
# This is only really required for AWS OpenID provider creation, however, this module
# will get the thumbprints, as long as both Gitlab and Github defaults are defined,
# regardless of whether an AWS OpenID is required or not.
data "tls_certificate" "github" {
  count = var.github_oidc_defaults != null ? 1 : 0
  url   = var.github_oidc_defaults.oidc_issuer
}

# GCP WIF
resource "google_iam_workload_identity_pool_provider" "github" {
  count                              = local.enable_gcp_wif && var.github_oidc_defaults != null ? 1 : 0
  workload_identity_pool_id          = local.gcp_wif_pool.workload_identity_pool_id
  workload_identity_pool_provider_id = var.github_oidc_defaults.oidc_name
  attribute_mapping = {
    "google.subject"          = "assertion.sub"
    "attribute.repository_id" = "assertion.repository_id"
    "attribute.repository"    = "assertion.repository"
    "attribute.actor"         = "assertion.actor"
    "attribute.ref_type"      = "assertion.ref_type"
    "attribute.ref"           = "assertion.ref"
    "attribute.environment"   = "assertion.environment"
  }
  oidc {
    issuer_uri        = var.github_oidc_defaults.oidc_issuer
    allowed_audiences = [var.github_oidc_defaults.oidc_audience]
  }
  attribute_condition = "attribute.subject==\"${local.github_oidc_subject}\""

  provider = google-beta
}

# Azure WIF
resource "azurerm_federated_identity_credential" "github" {
  count               = local.is_azure_mi && var.github_oidc_defaults != null ? 1 : 0
  name                = var.github_oidc_defaults.oidc_name
  resource_group_name = var.azure_wif.resource_group_name
  audience            = [var.github_oidc_defaults.oidc_audience]
  issuer              = var.github_oidc_defaults.oidc_issuer
  parent_id           = local.azure_service_account.id
  subject             = local.github_oidc_subject
}

# AWS WIF
resource "aws_iam_openid_connect_provider" "github" {
  count           = local.create_aws_wif && var.github_oidc_defaults != null ? 1 : 0
  url             = var.github_oidc_defaults.oidc_issuer
  client_id_list  = [var.github_oidc_defaults.oidc_audience]
  thumbprint_list = [data.tls_certificate.github[0].certificates[0].sha1_fingerprint]
}

data "aws_iam_openid_connect_provider" "github" {
  count = (local.enable_aws_wif && !local.create_aws_wif && var.github_oidc_defaults != null) ? 1 : 0
  url   = var.github_oidc_defaults.oidc_issuer
}
